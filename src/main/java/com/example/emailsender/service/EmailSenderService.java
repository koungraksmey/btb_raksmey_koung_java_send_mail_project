package com.example.emailsender.service;

import jakarta.mail.MessagingException;

public interface EmailSenderService {
    void sendEmail(String from,String to, String subject, String message);

}
