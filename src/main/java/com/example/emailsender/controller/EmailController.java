package com.example.emailsender.controller;

import com.example.emailsender.resource.EmailMessage;
import com.example.emailsender.service.EmailSenderService;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.thymeleaf.TemplateEngine;

@RestController
public class EmailController {

    private final EmailSenderService emailSenderService;

    public EmailController(EmailSenderService emailSenderService) {
        this.emailSenderService = emailSenderService;

    }

    @PostMapping("/send-email")
    public ResponseEntity<?> sendEmail(@RequestBody EmailMessage emailMessage) throws MessagingException {
        this.emailSenderService.sendEmail(emailMessage.getToEmail(),emailMessage.getSenderName(), emailMessage.getSubject(), emailMessage.getBody());
        return ResponseEntity.ok("Success");
    }
}
