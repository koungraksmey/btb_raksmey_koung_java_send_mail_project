package com.example.emailsender.resource;



public class EmailMessage {

    private String toEmail;
    private String senderName;
    private String subject;
    private String body;


    public EmailMessage(String toEmail, String senderName, String subject, String body) {
        this.toEmail = toEmail;
        this.senderName = senderName;
        this.subject = subject;
        this.body = body;
    }

    public String getToEmail() {
        return toEmail;
    }

    public void setToEmail(String toEmail) {
        this.toEmail = toEmail;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
